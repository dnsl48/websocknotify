use std::net::TcpListener;
use std::thread::spawn;
use tungstenite::server::accept;
use tungstenite::Message;
use std::os::unix::net::{UnixListener};

use std::collections::HashMap;
use std::time::{SystemTime, UNIX_EPOCH};

use std::sync::{Arc, RwLock};
use std::sync::mpsc::{sync_channel, SyncSender};

fn main() -> std::io::Result<()> {
    let q0: Arc<RwLock<HashMap<u128, SyncSender<()>>>> = Arc::new(RwLock::new(HashMap::new()));

    let q1 = q0.clone();
    spawn(move || {
        std::fs::remove_file("/tmp/websocknotify.sock").ok();
        let us = UnixListener::bind("/tmp/websocknotify.sock").unwrap();

        for ustream in us.incoming() {
            match ustream {
                Ok(_) => {
                    println!("Got a new US connection!");

                    let mut disconnected: Vec<u128> = Vec::new();

                    {
                        let senders = q1.read().unwrap();
                        for (id, sender) in senders.iter() {
                            if let Err(_) = sender.try_send(()) {
                                disconnected.push(*id);
                            }
                        }
                    }

                    if disconnected.len() > 0 {
                        let mut senders = q1.write().unwrap();
                        for id in disconnected {
                            senders.remove(&id);
                        }
                        
                    }
                }
                _ => ()
            }
        }
    });

    spawn(move || {
        let listen_to_address = std::env::var_os("WEBSOCKNOTIFY_HOST")
            .or(Some(std::ffi::OsString::from("localhost:8001")))
            .unwrap().into_string().unwrap();
        let server = TcpListener::bind(&listen_to_address).unwrap();

        println!("Listening to {}", listen_to_address);

        for wstream in server.incoming() {
            println!("Got a new WS connection!");
            let q2 = q0.clone();
            spawn (move || {
                let id = SystemTime::now()
                    .duration_since(UNIX_EPOCH)
                    .expect("Time went backwards").as_millis();
                let (tx, rx) = sync_channel(1);

                {
                    let mut senders = q2.write().unwrap();
                    senders.insert(id, tx);
                }

                let mut ws = accept(wstream.unwrap()).unwrap();

                loop {
                    let _ = rx.recv();
                    ws.write_message(Message::Text(String::new())).unwrap();
                }
            });
        }
    }).join().unwrap();

    Ok(())
}
